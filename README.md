# lisk-toolkitv1

delegate_passphrase_helper.py 
```
usage: 
    
    # Output in JSON
    python3 delegate_passphrase_helper.py encrypt
    Password: <enter desired password>
    Data: <data to encrypt>
    {"salt": "<salt>", "iv": "<iv>", "cipherText": "<cipherText>", "tag": "<tag>", "version": "1"}

    # Emulate commander output
    python3 delegate_passphrase_helper.py encrypt --commander
    Password: <enter desired password>
    Data: <data to encrypt>
    salt=<salt>&iv=<iv>&cipherText=<cipherText>&tag=<tag>&version=1

    # Input in JSON string
    python3 delegate_passphrase_helper.py decrypt
    Password: <enter desired password>
    Data: <data to encrypt>
    {"salt": "<salt>", "iv": "<iv>", "cipherText": "<cipherText>", "tag": "<tag>", "version": "1"}
    
    # Emulate commander input
    python3 delegate_passphrase_helper.py decrypt --commander
    Password: <enter desired password>
    Data: salt=<salt>&iv=<iv>&cipherText=<cipherText>&tag=<tag>&version=1
    'data plaintext'

```
