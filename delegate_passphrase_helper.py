import os
import json
import random
import hashlib
import logging
import getpass
import argparse
from Crypto.Cipher import AES

LOG = logging.getLogger(__name__)

def get_key_from_password(password, salt):
    """
    """
    PBKDF2_ITERATIONS = int(1e6)
    PBKDF2_KEYLEN = 32
    PBKDF2_HASH_FUNCTION = 'sha256'

    return hashlib.pbkdf2_hmac(PBKDF2_HASH_FUNCTION,
                               password,
                               salt,
                               PBKDF2_ITERATIONS,
                               PBKDF2_KEYLEN)

def ecryptAES256GCM(password, data):
    """
    """
    version = '1'
    salt = os.urandom(16)
    key = get_key_from_password(password, salt)

    iv = os.urandom(12)
    cipher = AES.new(key, AES.MODE_GCM, iv)

    ciphertext, tag = cipher.encrypt_and_digest(data)

    return {
        'salt': salt.hex(),
        'iv': iv.hex(),
        'cipherText': ciphertext.hex(),
        'tag': tag.hex(),
        'version': '1'
        }
    
def decryptAES256GCM(password, aes_gcm_obj):
    """
    """
    salt = bytes.fromhex(aes_gcm_obj['salt'])
    key = get_key_from_password(password, salt)

    iv = bytes.fromhex(aes_gcm_obj['iv'])
    tag = bytes.fromhex(aes_gcm_obj['tag'])
    cipher_text = bytes.fromhex(aes_gcm_obj['cipherText'])

    cipher = AES.new(key, AES.MODE_GCM, iv)
    plaintext = cipher.decrypt(cipher_text)

    try:
        cipher.verify(tag)
        return plaintext
    except ValueError:
        return 'Error verifying tag. Make sure password is correct'

def main(args):
    """
    """
    # Get the password for the data
    password = getpass.getpass("Password: ")
    passwd = bytearray()
    passwd.extend(password.encode())

    # Get the data to encrypt or decrypt
    data = getpass.getpass("Data: ")

    if args.opt == 'encrypt':

        dta = bytearray()
        dta.extend(data.encode())

        aes_gcm_obj = ecryptAES256GCM(passwd, dta)

        if args.commander:
            kvfy = ["{}={}".format(k, v) for k, v in aes_gcm_obj.items()]
            print("&".join(kvfy))
        else:
            print(json.dumps(aes_gcm_obj))

    elif args.opt == 'decrypt':

        if args.commander:
            dta = {x.split('=')[0]: x.split('=')[1] for x in data.split('&')}
        else:
            dta = json.loads(data)

        aes_gcm_resp = decryptAES256GCM(passwd, dta)

        if aes_gcm_resp:
            print(aes_gcm_resp)

def usage():
    return """
    
    # Output in JSON
    python3 delegate_passphrase_helper.py encrypt
    Password: <enter desired password>
    Data: <data to encrypt>
    {"salt": "<salt>", "iv": "<iv>", "cipherText": "<cipherText>", "tag": "<tag>", "version": "1"}


    # Emulate commander output
    python3 delegate_passphrase_helper.py encrypt --commander
    Password: <enter desired password>
    Data: <data to encrypt>
    salt=<salt>&iv=<iv>&cipherText=<cipherText>&tag=<tag>&version=1

    # Input in JSON string
    python3 delegate_passphrase_helper.py decrypt
    Password: <enter desired password>
    Data: <data to encrypt>
    {"salt": "<salt>", "iv": "<iv>", "cipherText": "<cipherText>", "tag": "<tag>", "version": "1"}
    
    # Emulate commander input
    python3 delegate_passphrase_helper.py decrypt --commander
    Password: <enter desired password>
    Data: salt=<salt>&iv=<iv>&cipherText=<cipherText>&tag=<tag>&version=1
    'data plaintext'
    """
if __name__ == '__main__':

    PARSER = argparse.ArgumentParser(description='LSK', usage=usage())

    
    PARSER.add_argument(dest='opt', action='store',
                        choices=['encrypt', 'decrypt'],
                        help='Action to take (encrypt, decrypt)')

    PARSER.add_argument('--version', dest='version',
                        default='1', action='store', help='')

    PARSER.add_argument('--commander', dest='commander',
                        action='store_true', help='Make it ugly')

    ARGS = PARSER.parse_args()

    logging.basicConfig(level=logging.INFO)
    logger = logging.getLogger()

    main(ARGS)
